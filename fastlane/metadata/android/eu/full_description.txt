F-Droiden konpilazioaren egoeraren APIaren Androiderako aplikazioa.

Ezaugarriak

- Huts egindako eta arrakastatsuak diren konpilazioak erakusten ditu
- Xehetasun-pantailan konpilazioaren erregistroa erakusten du
- Aplikazio bateko argitaratutako bertsioak erakusten ditu
- Aplikazio batek eguneraketa bat duen edo desgaituta dagoen erakusten du
- Ezarri aplikazioal gogoko gisa ordenarako
- Konpilazio berriaren egoeraren inguruan jakinarazi, aukeran, soilik zure gogoko aplikazioenak

Erabilitako F-Droid APIa:

Aplikazioak hurrengo F-Droid APIko amaiera-puntuak erabiltzen ditu:

- running.json: orain abian dagoen F-Droiden konpilazio-zerbitzaria, https://f-droid.org/repo/status/running.json
- build.json: F-Droiden konpilazio-zerbitzariko azken zikloa, https://f-droid.org/repo/status/build.json
- update.json: F-Droiden konpilazio-zerbitzariaren eguneraketaren azken egiaztatzea, https://f-droid.org/repo/status/update.json
- build.log: https://f-droid.org/repo/{id}_{versionCode}.log.gz
- argitaratutako paketearen informazioa: https://f-droid.org/api/v1/packages/{id}, example https://f-droid.org/api/v1/packages/org.fdroid.fdroid

Artelana

Logoa F-Droidetik eratorria: https://gitlab.com/fdroid/artwork
