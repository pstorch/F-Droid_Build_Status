Androidos alkalmazás az F-Droid összeállítási állapot API-jához.

Jellemzők

- Megjeleníti a sikertelen és sikeres összeállítások
- Megjeleníti az összeállítási naplót a részletek képernyőn
- Megjeleníti az alkalmazás közzétett verzióit
- Megmutatja, hogy egy alkalmazás frissítésre szorul-e vagy le van-e tiltva.
- Alkalmazások kedvencként való megjelölése a rendezéshez
- Értesítés az új összeállítás állapotáról, választható módon csak a kedvenc alkalmazások esetén

A használt F-Droid API:

Az alkalmazás a következő F-Droid API végpontokat használja:

- running.json: a jelenleg futó F-Droid összeállítási folyamat, https://f-droid.org/repo/status/running.json.
- build.json: a legutóbbi befejezett F-Droid összeállítási ciklus, https://f-droid.org/repo/status/build.json.
- update.json: az F-Droid összeállítási kiszolgáló legutóbbi frissítés-ellenőrzése, https://f-droid.org/repo/status/update.json.
- build.log: https://f-droid.org/repo/{id}_{versionCode}.log.gz
- közzétett csomaginformációk: https://f-droid.org/api/v1/packages/{id}, például https://f-droid.org/api/v1/packages/org.fdroid.fdroid.

Illusztráció

Logó az F-Droid alapján: https://gitlab.com/fdroid/artwork
