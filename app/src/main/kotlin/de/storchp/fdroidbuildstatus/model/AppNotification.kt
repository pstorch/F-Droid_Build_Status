package de.storchp.fdroidbuildstatus.model

data class AppNotification(
    val id: String,
    val versionCode: Long,
)