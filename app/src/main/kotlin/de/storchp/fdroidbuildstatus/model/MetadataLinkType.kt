package de.storchp.fdroidbuildstatus.model

private const val GITLAB_FDROIDDATA_BASE_URI = "https://gitlab.com/fdroid/fdroiddata/-/"

enum class MetadataLinkType(private val path: String?, val mimeType: String?) {
    ASK(null, null),
    YAML("raw/", "text/plain"),
    GITLAB("blob/", "text/html");

    val metadataBaseUri: String?
        get() = if (path != null) GITLAB_FDROIDDATA_BASE_URI + path else null

}