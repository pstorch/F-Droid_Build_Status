package de.storchp.fdroidbuildstatus.model

import de.storchp.fdroidbuildstatus.R

enum class BuildCycle(
    val isUpdatable: Boolean,
    val iconRes: Int,
    val labelRes: Int,
    val isFilter: Boolean,
    val isAppBuildFilter: Boolean,
) {
    RUNNING(true, R.drawable.ic_directions_run_24px, R.string.bottom_bar_running, true, true),
    BUILD(true, R.drawable.ic_build_24, R.string.bottom_bar_build, true, true),
    UPDATE(true, R.drawable.ic_upgrade_black_24dp, R.string.bottom_bar_update, true, true),
    NONE(false, 0, R.string.build_cycle, false, false),
    PUBLISH(false, 0, R.string.build_cycle, false, false);
}