package de.storchp.fdroidbuildstatus

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.storchp.fdroidbuildstatus.model.BuildCycle
import de.storchp.fdroidbuildstatus.model.BuildRun
import de.storchp.fdroidbuildstatus.utils.DrawableUtils
import de.storchp.fdroidbuildstatus.utils.FormatUtils

class MainAppListHeaderAdapter :
    RecyclerView.Adapter<MainAppListHeaderAdapter.HeaderViewHolder>() {

    private var buildRuns: Map<BuildCycle, BuildRun>? = null

    class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val headRunning: TextView = itemView.findViewById(R.id.build_status_head_running)
        private val startRunning: TextView = itemView.findViewById(R.id.build_status_start_running)
        private val startBuild: TextView = itemView.findViewById(R.id.build_status_start_build)
        private val startUpdate: TextView = itemView.findViewById(R.id.build_status_start_update)
        private val end: TextView = itemView.findViewById(R.id.build_status_end)
        private val endRunning: TextView = itemView.findViewById(R.id.build_status_end_running)
        private val endBuild: TextView = itemView.findViewById(R.id.build_status_end_build)
        private val endUpdate: TextView = itemView.findViewById(R.id.build_status_end_update)
        private val lastModifiedRunning: TextView =
            itemView.findViewById(R.id.build_status_last_modified_running)
        private val lastModifiedBuild: TextView =
            itemView.findViewById(R.id.build_status_last_modified_build)
        private val lastModifiedUpdate: TextView =
            itemView.findViewById(R.id.build_status_last_modified_update)
        private val buildsRunning: TextView =
            itemView.findViewById(R.id.build_status_builds_running)
        private val buildsBuild: TextView = itemView.findViewById(R.id.build_status_builds_build)
        private val buildsUpdate: TextView = itemView.findViewById(R.id.build_status_builds_update)

        fun bind(buildRuns: Map<BuildCycle, BuildRun>?) {
            buildRuns?.get(BuildCycle.RUNNING)?.apply {
                headRunning.text =
                    headRunning.resources.getString(
                        R.string.build_status_head_running_command,
                        subcommand
                    )
                startRunning.text = FormatUtils.formatShortDateTime(startTimestamp)
                endRunning.text =
                    if (endTimestamp > 0) FormatUtils.formatShortDateTime(
                        endTimestamp
                    ) else ""
                lastModifiedRunning.text = FormatUtils.formatShortDateTime(lastModified.time)
                buildsRunning.text = getNumberOfBuildsText()
            } ?: {
                headRunning.setText(R.string.build_status_head_running)
                startRunning.text = ""
                endRunning.text = ""
                lastModifiedRunning.text = ""
                buildsRunning.text = ""
            }

            buildRuns?.get(BuildCycle.BUILD)?.apply {
                startBuild.text = FormatUtils.formatShortDateTime(startTimestamp)
                endBuild.text = FormatUtils.formatShortDateTime(endTimestamp)
                lastModifiedBuild.text = FormatUtils.formatShortDateTime(lastModified.time)
                buildsBuild.text = getNumberOfBuildsText()
                if (isMaxBuildTimeReached) {
                    DrawableUtils.setCompoundDrawablesRight(
                        endBuild.context,
                        endBuild,
                        R.drawable.ic_error_outline_24px,
                        end.currentTextColor
                    )
                    endBuild.compoundDrawablePadding = 5
                } else {
                    endBuild.setCompoundDrawables(null, null, null, null)
                }
            } ?: {
                startBuild.text = ""
                endBuild.text = ""
                endBuild.setCompoundDrawables(null, null, null, null)
                lastModifiedBuild.text = ""
                buildsBuild.text = ""
            }

            buildRuns?.get(BuildCycle.UPDATE)?.apply {
                startUpdate.text = FormatUtils.formatShortDateTime(startTimestamp)
                endUpdate.text = FormatUtils.formatShortDateTime(endTimestamp)
                lastModifiedUpdate.text = FormatUtils.formatShortDateTime(lastModified.time)
                buildsUpdate.text = getNumberOfBuildsText()
            } ?: {
                startUpdate.text = ""
                endUpdate.text = ""
                lastModifiedUpdate.text = ""
                buildsUpdate.text = ""
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeaderViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.main_list_header, parent, false)
        return HeaderViewHolder(view)
    }

    override fun onBindViewHolder(holder: HeaderViewHolder, position: Int) {
        holder.bind(buildRuns)
    }

    override fun getItemCount(): Int {
        return 1
    }

    fun setBuildStatusText(buildRuns: Map<BuildCycle, BuildRun>) {
        this.buildRuns = buildRuns
        notifyDataSetChanged()
    }

}