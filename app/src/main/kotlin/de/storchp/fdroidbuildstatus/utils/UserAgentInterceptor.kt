package de.storchp.fdroidbuildstatus.utils

import android.os.Build.VERSION
import de.storchp.fdroidbuildstatus.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

/* This interceptor adds a custom User-Agent. */
class UserAgentInterceptor : Interceptor {
    private val userAgent: String =
        BuildConfig.APPLICATION_ID + "/" + BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE + "); Android " + VERSION.RELEASE + "/" + VERSION.SDK_INT

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request().newBuilder()
                .header("User-Agent", userAgent)
                .build()
        )
    }
}