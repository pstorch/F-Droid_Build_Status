package de.storchp.fdroidbuildstatus.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import de.storchp.fdroidbuildstatus.DetailActivity
import de.storchp.fdroidbuildstatus.EXTRA_BUILD_ITEM_ID
import de.storchp.fdroidbuildstatus.MainActivity
import de.storchp.fdroidbuildstatus.R


object NotificationUtils {

    private val TAG = NotificationUtils::class.java.simpleName
    private const val CHANNEL_ID = "general"

    private const val NEW_BUILD_NOTIFICATION_ID = 0

    fun createNewBuildFavoritesNotification(context: Context, text: String, names: List<String>) {
        var style = NotificationCompat.InboxStyle()
        names.take(5).forEach {
            style = style.addLine(it)
        }
        if (names.size > 5) {
            style = style.addLine("…")
        }
        createNewBuildNotification(context, text, style)
    }

    fun createNewPublishFavoriteNotification(context: Context, text: String, id: String) {
        val notificationManager = getNotificationManager(context)
        if (!notificationManager.areNotificationsEnabled()) {
            Log.w(TAG, "notifications disabled")
            return
        }
        val intent = Intent(context, DetailActivity::class.java).apply {
            putExtra(EXTRA_BUILD_ITEM_ID, id)
        }
        val pIntent = PendingIntent.getActivity(
            context,
            System.currentTimeMillis().toInt(),
            intent,
            PendingIntent.FLAG_IMMUTABLE
        )
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentTitle(text)
            .setSmallIcon(R.drawable.ic_fdroid_buildstatus)
            .setContentIntent(pIntent)
            .setAutoCancel(true)

        notificationManager.notify(id, NEW_BUILD_NOTIFICATION_ID, builder.build())
    }

    private fun createNewBuildNotification(
        context: Context,
        text: String,
        style: NotificationCompat.InboxStyle?
    ) {
        val notificationManager = getNotificationManager(context)
        if (!notificationManager.areNotificationsEnabled()) {
            Log.w(TAG, "notifications disabled")
            return
        }
        val pIntent = PendingIntent.getActivity(
            context,
            System.currentTimeMillis().toInt(),
            Intent(context, MainActivity::class.java),
            PendingIntent.FLAG_IMMUTABLE
        )
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentTitle(text)
            .setSmallIcon(R.drawable.ic_fdroid_buildstatus)
            .setContentIntent(pIntent)
            .setAutoCancel(true)
            .setStyle(style)

        notificationManager.notify(NEW_BUILD_NOTIFICATION_ID, builder.build())
    }

    fun cancelNewBuildNotification(context: Context) {
        getNotificationManager(context).cancel(NEW_BUILD_NOTIFICATION_ID)
    }

    private fun getNotificationManager(context: Context): NotificationManager {
        return context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    fun areNotificationsEnabled(context: Context): Boolean {
        return getNotificationManager(context).areNotificationsEnabled()
    }

    fun createNotificationChannel(context: Context) {
        val channel = NotificationChannel(
            CHANNEL_ID,
            context.getString(R.string.channel_name),
            NotificationManager.IMPORTANCE_DEFAULT
        )
        channel.description = context.getString(R.string.channel_description)
        channel.enableVibration(true)
        getNotificationManager(context).createNotificationChannel(channel)
    }

}