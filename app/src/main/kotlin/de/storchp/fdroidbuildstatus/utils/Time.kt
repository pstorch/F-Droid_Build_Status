package de.storchp.fdroidbuildstatus.utils

object Time {
    const val ONE_HOUR = (1000 * 60 * 60).toLong()
    const val FIVE_HOURS = ONE_HOUR * 5
}