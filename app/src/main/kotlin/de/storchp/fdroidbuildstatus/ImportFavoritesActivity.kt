package de.storchp.fdroidbuildstatus

import android.content.ClipData
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity

class ImportFavoritesActivity : AppCompatActivity() {

    private val openCsv = registerForActivityResult(
        ActivityResultContracts.OpenDocument()
    ) { result ->
        if (result != null) {
            val intent = Intent(this, ImportAppsAsFavouritesActivity::class.java).apply {
                clipData = ClipData.newRawUri(result.lastPathSegment, result)
            }
            startActivity(intent)
        }
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        openCsv.launch(
            arrayOf(
                "application/csv",
                "application/x-csv",
                "text/csv",
                "text/comma-separated-values",
                "text/x-comma-separated-values",
                "text/tab-separated-values"
            )
        )
    }

}
