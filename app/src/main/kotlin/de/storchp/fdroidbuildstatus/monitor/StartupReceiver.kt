package de.storchp.fdroidbuildstatus.monitor

import android.content.Context
import android.content.Intent
import android.util.Log
import dagger.hilt.android.AndroidEntryPoint
import de.storchp.fdroidbuildstatus.utils.DaggerBroadcastReceiver
import de.storchp.fdroidbuildstatus.utils.PreferencesService
import javax.inject.Inject

private const val TAG = "StartupReceiver"

@AndroidEntryPoint
class StartupReceiver : DaggerBroadcastReceiver() {

    @Inject
    lateinit var preferencesService: PreferencesService

    override fun onReceive(context: Context, intent: Intent?) {
        super.onReceive(context, intent)
        if (Intent.ACTION_BOOT_COMPLETED == intent?.action) {
            scheduleMonitorJob(context, preferencesService)
        } else {
            Log.d(TAG, "received unsupported Intent $intent")
        }
    }

}