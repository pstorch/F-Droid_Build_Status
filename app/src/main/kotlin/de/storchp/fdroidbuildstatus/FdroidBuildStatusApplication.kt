package de.storchp.fdroidbuildstatus

import android.app.Application
import android.content.Context
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import com.google.android.material.color.DynamicColors
import dagger.hilt.EntryPoint
import dagger.hilt.EntryPoints
import dagger.hilt.InstallIn
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.components.SingletonComponent
import de.storchp.fdroidbuildstatus.monitor.scheduleMonitorJob
import de.storchp.fdroidbuildstatus.utils.NotificationUtils
import de.storchp.fdroidbuildstatus.utils.PreferencesService
import de.storchp.fdroidbuildstatus.utils.attachExceptionHandler
import javax.inject.Inject

@HiltAndroidApp
class FdroidBuildStatusApplication : Application(), Configuration.Provider {
    @Inject
    lateinit var preferencesService: PreferencesService

    @EntryPoint
    @InstallIn(SingletonComponent::class)
    interface HiltWorkerFactoryEntryPoint {
        fun workerFactory(): HiltWorkerFactory
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        attachExceptionHandler(this)
    }


    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
        NotificationUtils.createNotificationChannel(this)
        scheduleMonitorJob(this, preferencesService)
        preferencesService.applyDefaultNightMode()
    }

    override val workManagerConfiguration: Configuration = Configuration.Builder()
        .setWorkerFactory(
            EntryPoints.get(this, HiltWorkerFactoryEntryPoint::class.java).workerFactory()
        )
        .build()

}