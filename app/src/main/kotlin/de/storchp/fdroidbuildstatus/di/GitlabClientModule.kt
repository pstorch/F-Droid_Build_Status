package de.storchp.fdroidbuildstatus.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.storchp.fdroidbuildstatus.adapter.gitlab.GitlabClient
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class GitlabClientModule {

    @Singleton
    @Provides
    fun provideRSAPIClient(): GitlabClient {
        return GitlabClient("https://gitlab.com")
    }

}