package de.storchp.fdroidbuildstatus.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import de.storchp.fdroidbuildstatus.adapter.fdroid.FdroidClient
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class FdroidClientModule {

    @Singleton
    @Provides
    fun provideRSAPIClient(): FdroidClient {
        return FdroidClient("https://f-droid.org")
    }

}