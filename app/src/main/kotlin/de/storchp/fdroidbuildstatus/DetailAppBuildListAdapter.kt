package de.storchp.fdroidbuildstatus

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import de.storchp.fdroidbuildstatus.databinding.ItemDetailAppbuildBinding
import de.storchp.fdroidbuildstatus.model.AppBuild
import de.storchp.fdroidbuildstatus.model.BuildCycle
import de.storchp.fdroidbuildstatus.utils.DrawableUtils

class DetailAppBuildListAdapter(
    private val context: Activity,
    appBuildsByVersionCodeAndStatus: Map<String, List<AppBuild>>
) : BaseAdapter() {
    private val appBuildsByVersionCodeAndStatus: List<List<AppBuild>>?

    init {
        this.appBuildsByVersionCodeAndStatus = ArrayList(appBuildsByVersionCodeAndStatus.values)
    }

    fun getItemForPosition(position: Int): AppBuild? {
        return appBuildsByVersionCodeAndStatus?.get(
            position
        )?.get(0)
    }

    override fun getCount(): Int {
        return appBuildsByVersionCodeAndStatus?.size ?: 0
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var rowView = convertView
        val binding: ItemDetailAppbuildBinding
        val inflater = context.layoutInflater
        if (rowView == null) {
            binding = ItemDetailAppbuildBinding.inflate(inflater, parent, false)
            rowView = binding.root
            rowView.setTag(binding)
        } else {
            binding = rowView.tag as ItemDetailAppbuildBinding
        }

        val appBuilds = appBuildsByVersionCodeAndStatus!![position]
        val item = appBuilds[0]
        binding.version.text = item.fullVersion
        DrawableUtils.setIconWithTint(
            context,
            binding.statusIcon,
            item.status.iconRes,
            binding.version.currentTextColor
        )
        setBuildTypIcon(
            binding.buildRunTypeRunning,
            appBuilds,
            BuildCycle.RUNNING,
            binding.version.currentTextColor
        )
        setBuildTypIcon(
            binding.buildRunTypeFinished,
            appBuilds,
            BuildCycle.BUILD,
            binding.version.currentTextColor
        )
        setBuildTypIcon(
            binding.buildRunTypeUpdate,
            appBuilds,
            BuildCycle.UPDATE,
            binding.version.currentTextColor
        )
        return rowView
    }

    private fun setBuildTypIcon(
        bindingBuild: ImageView,
        builds: List<AppBuild>,
        running: BuildCycle,
        color: Int
    ) {
        bindingBuild.visibility = if (builds
                .any { (_, _, buildCycle): AppBuild -> buildCycle === running }
        ) View.VISIBLE else View.GONE
        DrawableUtils.setIconWithTint(context, bindingBuild, running.iconRes, color)
    }
}