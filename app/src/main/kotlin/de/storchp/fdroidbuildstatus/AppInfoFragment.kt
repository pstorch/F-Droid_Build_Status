package de.storchp.fdroidbuildstatus

import android.app.Dialog
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import dagger.hilt.android.AndroidEntryPoint
import de.storchp.fdroidbuildstatus.adapter.db.DbAdapter
import de.storchp.fdroidbuildstatus.databinding.ActivityAboutBinding
import de.storchp.fdroidbuildstatus.model.BuildCycle
import de.storchp.fdroidbuildstatus.model.BuildRun
import de.storchp.fdroidbuildstatus.utils.FormatUtils
import javax.inject.Inject

@AndroidEntryPoint
class AppInfoFragment : DialogFragment() {

    @Inject
    lateinit var dbAdapter: DbAdapter

    private lateinit var binding: ActivityAboutBinding
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val fragmentActivity = requireActivity()
        val builder = AlertDialog.Builder(fragmentActivity)
        builder.setIcon(R.mipmap.ic_launcher)
            .setTitle(R.string.app_info_title)
            .setPositiveButton(android.R.string.ok, null)
        binding = ActivityAboutBinding.inflate(
            layoutInflater, null, false
        )
        binding.aboutVersionText.text = String.format(
            "%s (%s)",
            BuildConfig.VERSION_NAME,
            BuildConfig.VERSION_CODE
        )
        binding.aboutInfo.movementMethod = LinkMovementMethod.getInstance()
        val buildRuns = dbAdapter.loadBuildRuns()
        binding.aboutLastUpdateBuildText.text = getLastUpdate(buildRuns, BuildCycle.BUILD)
        binding.aboutLastUpdateRunningText.text = getLastUpdate(buildRuns, BuildCycle.RUNNING)
        builder.setView(binding.root)
        return builder.create()
    }

    private fun getLastUpdate(buildRuns: Map<BuildCycle, BuildRun>, runType: BuildCycle): String {
        return buildRuns[runType]?.let {
            it.lastUpdated.time.let { it1 -> FormatUtils.formatShortDateTime(it1) }
        } ?: ""
    }
}