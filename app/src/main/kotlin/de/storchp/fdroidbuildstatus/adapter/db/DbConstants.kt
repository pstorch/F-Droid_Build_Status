package de.storchp.fdroidbuildstatus.adapter.db

object DbConstants {
    object Apps {
        const val ID = "id"
        const val NAME = "name"
        const val FAVOURITE = "favourite"
        const val DISABLED = "disabled"
        const val NEEDS_UPDATE = "needs_update"
        const val SOURCE_CODE = "sourceCode"
        const val ARCHIVED = "archived"
        const val NO_PACKAGES = "noPackages"
        const val NO_UPDATE_CHECK = "noUpdateCheck"
    }

    object Builds {
        const val BUILD_RUN_TYPE = "buildRunType"
        const val ID = "id"
        const val VERSION = "version"
        const val VERSION_CODE = "versionCode"
        const val STATUS = "status"
        const val LOG = "error" // was previous only error log
    }

    object Versions {
        const val ID = "id"
        const val VERSION_CODE = "versionCode"
        const val VERSION_NAME = "versionName"
    }

    object BuildRuns {
        const val BUILD_RUN_TYPE = "buildRunType"
        const val START = "start"
        const val END = "end"
        const val LAST_MODIFIED = "lastModified"
        const val LAST_UPDATED = "lastUpdated"
        const val MAX_BUILD_TIME_REACHED = "maxBuildTimeReached"
        const val SUBCOMMAND = "subcommand"
        const val DATA_COMMIT_ID = "commitId"
    }

    object Disabled {
        const val ID = "id"
    }

    object NeedsUpdate {
        const val ID = "id"
    }

    object Notifications {
        const val BUILD_RUN_TYPE = "buildRunType"
        const val START = "start"
        const val ID = "id"
        const val VERSION_CODE = "versionCode"
    }

    const val DATABASE_NAME = "fdroidbuildstatus.db"
    const val DATABASE_TABLE_APPS = "apps"
    const val DATABASE_TABLE_BUILD_RUNS = "build_runs"
    const val DATABASE_TABLE_BUILDS = "builds"
    const val DATABASE_TABLE_VERSIONS = "versions"
    const val DATABASE_TABLE_DISABLED = "disabled"
    const val DATABASE_TABLE_NEEDS_UPDATE = "needs_update"
    const val DATABASE_TABLE_NOTIFICATIONS = "notifications"
    const val COUNTER = "COUNTER"
}