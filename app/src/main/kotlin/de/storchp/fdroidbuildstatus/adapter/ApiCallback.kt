package de.storchp.fdroidbuildstatus.adapter

fun interface ApiCallback<T> {
    fun onResponse(response: ApiResponse<T?>)
}