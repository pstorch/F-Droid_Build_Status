package de.storchp.fdroidbuildstatus.adapter.fdroid

import de.storchp.fdroidbuildstatus.model.BuildStatus
import java.util.Objects

open class BuildItem(val id: String, val versionCode: Long, val status: BuildStatus) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BuildItem) return false
        return id == other.id && versionCode == other.versionCode
    }

    override fun hashCode(): Int {
        return Objects.hash(id, versionCode)
    }
}