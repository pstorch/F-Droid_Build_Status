package de.storchp.fdroidbuildstatus.adapter.gitlab

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

internal interface GitlabAPI {
    @GET("/fdroid/fdroiddata/-/raw/master/metadata/{id}.yml")
    fun getFdroidDataMetadata(@Path("id") id: String?): Call<Metadata?>?
}