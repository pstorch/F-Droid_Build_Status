package de.storchp.fdroidbuildstatus.adapter.fdroid

class WebsiteBuildStatus {
    var commandLine: List<String>? = null
    var endTimestamp: Long = 0
    var startTimestamp: Long = 0
}