package de.storchp.fdroidbuildstatus.adapter.fdroid

class BuildResult : RunningResult() {
    private val failedBuilds = FailedBuilds()
    val successfulBuildIds = mutableSetOf<SuccessBuildIdItem>()
    private val isMaxBuildTimeReached = false
    fun failedBuildItems(): Set<FailedBuildItem> {
        return failedBuilds.getFailedBuildItems()
    }

    fun allBuilds(): List<BuildItem> {
        return failedBuildItems().toList() + successfulBuildIds.toList()
    }

    override fun getMaxBuildTimeReached(): Boolean {
        return isMaxBuildTimeReached
    }

    override fun getBuildItems(): Set<BuildItem?> {
        return HashSet(allBuilds())
    }

}