package de.storchp.fdroidbuildstatus.adapter.fdroid

import android.text.TextUtils
import de.storchp.fdroidbuildstatus.utils.FormatUtils

data class Index @JvmOverloads constructor(
    var apps: List<App> = ArrayList()
) {

    data class App @JvmOverloads constructor(
        var name: String? = null,
        var packageName: String? = null,
        var sourceCode: String? = null,
        var localized: Map<String, LocalizedName> = HashMap()
    ) {
        val appName: String?
            get() = if (!TextUtils.isEmpty(name)) {
                name
            } else localized.entries
                .filter { (key, value): Map.Entry<String, LocalizedName> ->
                    !TextUtils.isEmpty(value.name) && key.startsWith("en")
                }
                .map { (_, value): Map.Entry<String, LocalizedName> -> value.name }
                .firstOrNull()
                ?: fallbackName()

        private fun fallbackName(): String {
            return localized.values
                .map { obj: LocalizedName -> obj.name }
                .firstOrNull { n: String? -> !TextUtils.isEmpty(n) }
                ?: FormatUtils.getNameFromSource(sourceCode)
        }

    }

    data class LocalizedName @JvmOverloads constructor(
        var name: String? = null
    )

}