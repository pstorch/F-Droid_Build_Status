package de.storchp.fdroidbuildstatus.adapter.fdroid

data class PublishedVersions @JvmOverloads constructor(
    var packageName: String,
    var suggestedVersionCode: String? = null,
    var packages: List<PublishedPackage> = ArrayList()
) {

    data class PublishedPackage(
        var versionName: String,
        var versionCode: Long,
    )
}