package de.storchp.fdroidbuildstatus.adapter

class ApiResponse<T> private constructor(
    private val value: T,
    private val status: Status,
    private val errorMessage: String?
) {
    fun value(): T {
        return value
    }

    fun status(): Status {
        return status
    }

    fun errorMessage(): String? {
        return errorMessage
    }

    val isSuccess: Boolean
        get() = status == Status.SUCCESS

    enum class Status {
        NOT_FOUND, SUCCESS, ERROR
    }

    companion object {
        fun <T> error(errorMessage: String?): ApiResponse<T?> {
            return ApiResponse(null, Status.ERROR, errorMessage)
        }

        fun <T> error(status: Status, errorMessage: String?): ApiResponse<T?> {
            return ApiResponse(null, status, errorMessage)
        }

        fun <T> success(value: T): ApiResponse<T> {
            return ApiResponse(value, Status.SUCCESS, null)
        }
    }
}