package de.storchp.fdroidbuildstatus.adapter.fdroid

import com.google.gson.*
import com.google.gson.annotations.JsonAdapter
import java.lang.reflect.Type
import java.util.*

@JsonAdapter(FailedBuilds.Deserializer::class)
class FailedBuilds {
    private val failedBuildItems: MutableSet<FailedBuildItem> = HashSet()
    fun getFailedBuildItems(): Set<FailedBuildItem> {
        return failedBuildItems
    }

    class Deserializer : JsonDeserializer<FailedBuilds?> {
        override fun deserialize(
            json: JsonElement,
            typeOfT: Type,
            context: JsonDeserializationContext
        ): FailedBuilds? {
            return try {
                val failedBuilds = FailedBuilds()
                if (json.isJsonArray) {
                    val array = json.asJsonArray
                    for (i in 0 until array.size()) {
                        val element = array[i]
                        if (element.isJsonArray) {
                            val elements = element.asJsonArray
                            failedBuilds.failedBuildItems.add(
                                FailedBuildItem(
                                    elements[0].asString,
                                    elements[1].asLong
                                )
                            )
                        }
                    }
                } else if (json.isJsonObject) {
                    val jsonObject = json.asJsonObject
                    failedBuilds.failedBuildItems.addAll(jsonObject.entrySet()
                        .mapNotNull { (key, element): Map.Entry<String?, JsonElement> ->
                            if (element.isJsonArray && element.asJsonArray.size() > 0) {
                                return@mapNotNull FailedBuildItem(
                                    key!!,
                                    element.asJsonArray[0].asLong
                                )
                            }
                            null
                        })
                }
                failedBuilds
            } catch (_: Exception) {
                null
            }
        }
    }
}