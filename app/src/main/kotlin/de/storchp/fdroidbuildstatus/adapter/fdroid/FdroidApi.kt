package de.storchp.fdroidbuildstatus.adapter.fdroid

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

internal interface FdroidApi {
    @get:GET("/repo/status/build.json")
    @get:Headers("Cache-Control: no-cache")
    val build: Call<BuildResult>

    @get:GET("/repo/status/running.json")
    @get:Headers("Cache-Control: no-cache")
    val running: Call<RunningResult>

    @get:GET("/repo/status/deploy-to-f-droid.org.json")
    @get:Headers("Cache-Control: no-cache")
    val websiteBuildStatus: Call<WebsiteBuildStatus>

    @get:GET("/repo/status/update.json")
    val update: Call<UpdateResult>

    @GET("/repo/{id}_{versionCode}.log.gz")
    fun getBuildLog(
        @Path("id") id: String,
        @Path("versionCode") versionCode: Long
    ): Call<ResponseBody>

    @GET("/api/v1/packages/{id}")
    fun getPublishedPackage(@Path("id") id: String?): Call<PublishedVersions>

    @get:GET("/repo/index-v1.jar")
    val index: Call<ResponseBody>
}