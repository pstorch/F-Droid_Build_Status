package de.storchp.fdroidbuildstatus.adapter.fdroid

import de.storchp.fdroidbuildstatus.model.BuildStatus

class MissingBuildItem(id: String, versionCode: Long) : BuildItem(
    id, versionCode, BuildStatus.MISSING
)