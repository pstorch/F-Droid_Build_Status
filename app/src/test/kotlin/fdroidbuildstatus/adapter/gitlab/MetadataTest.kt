package fdroidbuildstatus.adapter.gitlab

import de.storchp.fdroidbuildstatus.adapter.gitlab.Metadata
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class MetadataTest {
    @Test
    fun testHighestMetadataVersionCode() {
        val metadata = Metadata(
            builds = setOf(
                Metadata.Build(9, "1.2.3"),
                Metadata.Build(11, "1.3.1")
            )
        )
        assertThat(metadata.highestVersion?.versionCode).isEqualTo(11)
    }
}