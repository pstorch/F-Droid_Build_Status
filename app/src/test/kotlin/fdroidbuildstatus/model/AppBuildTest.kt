package fdroidbuildstatus.model

import de.storchp.fdroidbuildstatus.model.AppBuild
import de.storchp.fdroidbuildstatus.model.BuildCycle
import de.storchp.fdroidbuildstatus.model.BuildStatus
import de.storchp.fdroidbuildstatus.model.MetadataLinkType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class AppBuildTest {
    @Test
    fun getFullVersion() {
        val appBuild = AppBuild(
            versionCode = 123,
            versionName = "1.2.3",
            buildCycle = BuildCycle.NONE,
            status = BuildStatus.SUCCESS
        )
        assertThat(appBuild.fullVersion).isEqualTo("123 - 1.2.3")
    }

    @Test
    fun getFullVersionWithoutVersionName() {
        val appBuild = AppBuild(
            versionCode = 123,
            buildCycle = BuildCycle.NONE,
            status = BuildStatus.SUCCESS
        )
        assertThat(appBuild.fullVersion).isEqualTo("123")
    }

    @Test
    fun getMetadataUriMasterNoMetadataPath() {
        val appBuild = AppBuild(1)
        assertThat(
            appBuild.getMetadataUri(
                MetadataLinkType.GITLAB,
                "com.example.app-id"
            )
        )
            .isEqualTo("https://gitlab.com/fdroid/fdroiddata/-/blob/master/metadata/com.example.app-id.yml")
    }

    @Test
    fun getMetadataUriMasterNoMetadataPathRawYaml() {
        val appBuild = AppBuild(1)
        assertThat(
            appBuild.getMetadataUri(
                MetadataLinkType.YAML,
                "com.example.app-id"
            )
        )
            .isEqualTo("https://gitlab.com/fdroid/fdroiddata/-/raw/master/metadata/com.example.app-id.yml")
    }

    @Test
    fun getMetadataUriCommitIdNoMetadataPath() {
        val appBuild = AppBuild(
            versionCode = 1,
            buildCycle = BuildCycle.NONE,
            status = BuildStatus.MISSING,
            dataCommitId = "commitId"
        )
        assertThat(
            appBuild.getMetadataUri(
                MetadataLinkType.GITLAB,
                "com.example.app-id"
            )
        )
            .isEqualTo("https://gitlab.com/fdroid/fdroiddata/-/blob/commitId/metadata/com.example.app-id.yml")
    }
}