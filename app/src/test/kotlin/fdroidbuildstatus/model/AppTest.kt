package fdroidbuildstatus.model

import de.storchp.fdroidbuildstatus.R
import de.storchp.fdroidbuildstatus.model.App
import de.storchp.fdroidbuildstatus.model.AppBuild
import de.storchp.fdroidbuildstatus.model.BuildCycle
import de.storchp.fdroidbuildstatus.model.BuildStatus
import de.storchp.fdroidbuildstatus.model.MetadataLinkType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class AppTest {
    @Test
    fun getDisplayNameWithIdAndName() {
        val app = App("some.id", "Some Name")
        assertThat(app.displayName).isEqualTo("Some Name")
    }

    @Test
    fun getDisplayNameWithIdAndNoName() {
        val app = App("some.id")
        assertThat(app.displayName).isEqualTo("some.id")
    }

    @Test
    fun getFavouriteIconForFavoriteApp() {
        val app = App(
            "some.id",
            "name",
            true
        )
        assertThat(app.favouriteIcon).isEqualTo(R.drawable.ic_favourite_24px)
    }

    @Test
    fun getFavouriteIconForNonFavoriteApp() {
        val app = App(
            "some.id",
            "name",
            false
        )
        assertThat(app.favouriteIcon).isEqualTo(R.drawable.ic_no_favourite_24px)
    }

    @Test
    fun getFdroidUri() {
        val app = App("some.id")
        assertThat(app.fdroidUri).isEqualTo("https://f-droid.org/packages/some.id")
    }

    @Test
    fun getMetadataUri() {
        val app = App("some.id")
        assertThat(app.getMetadataUri(MetadataLinkType.YAML))
            .isEqualTo("https://gitlab.com/fdroid/fdroiddata/-/raw/master/metadata/some.id.yml")
    }

    @Test
    fun getAppBuildsByVersionCodeAndStatus() {
        val build1SuccessRunning =
            AppBuild(1, "name", BuildCycle.RUNNING, BuildStatus.SUCCESS)
        val build1SuccessFinished = AppBuild(1, "name", BuildCycle.BUILD, BuildStatus.SUCCESS)
        val build2SuccessRunning =
            AppBuild(2, "name", BuildCycle.RUNNING, BuildStatus.SUCCESS)
        val build2FailedFinished = AppBuild(2, "name", BuildCycle.BUILD, BuildStatus.FAILED)
        val build3SuccessRunning =
            AppBuild(3, "name", BuildCycle.RUNNING, BuildStatus.SUCCESS)
        val app = App(
            id = "some.id",
            name = "name",
            favourite = false,
            disabled = false,
            needsUpdate = false,
            archived = false,
            noPackages = false,
            noUpdateCheck = false,
            appBuilds = mutableSetOf(
                build1SuccessRunning,
                build1SuccessFinished,
                build2SuccessRunning,
                build2FailedFinished,
                build3SuccessRunning
            )
        )
        val result = app.appBuildsByVersionCodeAndStatus
        assertThat(result["1SUCCESS"])
            .containsExactlyInAnyOrder(build1SuccessRunning, build1SuccessFinished)
        assertThat(result["2SUCCESS"])
            .containsExactlyInAnyOrder(build2SuccessRunning)
        assertThat(result["2FAILED"]).containsExactlyInAnyOrder(build2FailedFinished)
        assertThat(result["3SUCCESS"])
            .containsExactlyInAnyOrder(build3SuccessRunning)
    }
}