package fdroidbuildstatus.utils

import de.storchp.fdroidbuildstatus.utils.LoglinesBuffer
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class LoglinesBufferTest {
    @Test
    fun testBufferEmpty() {
        val buffer = LoglinesBuffer(5)
        for (line in buffer) {
            Assertions.fail<Any>()
        }
        org.assertj.core.api.Assertions.assertThat(buffer.toString()).isEmpty()
    }

    @Test
    fun testBufferLessLines() {
        val buffer = LoglinesBuffer(5)
        buffer.add("line1")
        buffer.add("line2")
        buffer.add("line3")
        var lineNr = 1
        for (line in buffer) {
            org.assertj.core.api.Assertions.assertThat(line).isEqualTo("line$lineNr")
            lineNr++
        }
        org.assertj.core.api.Assertions.assertThat(lineNr).isEqualTo(4)
        org.assertj.core.api.Assertions.assertThat(buffer.toString())
            .isEqualTo("line1\nline2\nline3")
    }

    @Test
    fun testBufferExactLines() {
        val buffer = LoglinesBuffer(5)
        buffer.add("line1")
        buffer.add("line2")
        buffer.add("line3")
        buffer.add("line4")
        buffer.add("line5")
        var lineNr = 1
        for (line in buffer) {
            org.assertj.core.api.Assertions.assertThat(line).isEqualTo("line$lineNr")
            lineNr++
        }
        org.assertj.core.api.Assertions.assertThat(lineNr).isEqualTo(6)
        org.assertj.core.api.Assertions.assertThat(buffer.toString())
            .isEqualTo("line1\nline2\nline3\nline4\nline5")
    }

    @Test
    fun testBufferMoreLines() {
        val buffer = LoglinesBuffer(5)
        buffer.add("line1")
        buffer.add("line2")
        buffer.add("line3")
        buffer.add("line4")
        buffer.add("line5")
        buffer.add("line6")
        buffer.add("line7")
        var lineNr = 1
        for (line in buffer) {
            org.assertj.core.api.Assertions.assertThat(line).isEqualTo("line" + (lineNr + 2))
            lineNr++
        }
        org.assertj.core.api.Assertions.assertThat(buffer.toString())
            .isEqualTo("line3\nline4\nline5\nline6\nline7")
    }
}