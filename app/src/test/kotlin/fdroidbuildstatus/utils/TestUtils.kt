package fdroidbuildstatus.utils

import okio.Buffer
import java.util.Objects

object TestUtils {
    fun fromFile(filename: String?): Buffer {
        return Buffer().readFrom(
            Objects.requireNonNull(
                TestUtils::class.java.classLoader
            ).getResourceAsStream(filename)
        )
    }
}