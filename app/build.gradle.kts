import java.io.ByteArrayOutputStream
import java.nio.charset.Charset

plugins {
    kotlin("kapt")
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.android.junit5)
    alias(libs.plugins.hilt.android)
}

abstract class GitVersionValueSource : ValueSource<Int, ValueSourceParameters.None> {
    @get:Inject
    abstract val execOperations: ExecOperations

    override fun obtain(): Int {
        val output = ByteArrayOutputStream()
        execOperations.exec {
            commandLine("git", "rev-list", "HEAD", "--count")
            standardOutput = output
        }
        return Integer.valueOf(String(output.toByteArray(), Charset.defaultCharset()).trim())
    }
}

val gitVersionProvider = providers.of(GitVersionValueSource::class) {}
val gitVersion = gitVersionProvider.get()

android {

    defaultConfig {
        testInstrumentationRunnerArguments += mapOf("runnerBuilder" to "de.mannodermaus.junit5.AndroidJUnit5Builder")
        applicationId = "de.storchp.fdroidbuildstatus"
        compileSdk = 35
        minSdk = 26
        targetSdk = 35
        versionCode = 112
        versionName = "5.8.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    dependenciesInfo {
        // Disables dependency metadata when building APKs.
        includeInApk = false
        // Disables dependency metadata when building Android App Bundles.
        includeInBundle = false
    }

    signingConfigs {
        register("nightly") {
            if (System.getProperty("nightly_store_file") != null) {
                storeFile = file(System.getProperty("nightly_store_file"))
                storePassword = System.getProperty("nightly_store_password")
                keyAlias = System.getProperty("nightly_key_alias")
                keyPassword = System.getProperty("nightly_key_password")
            }
        }
        register("release") {
            if (System.getProperty("release_store_file") != null) {
                storeFile = file(System.getProperty("release_store_file"))
                storePassword = System.getProperty("release_store_password")
                keyAlias = System.getProperty("release_key_alias")
                keyPassword = System.getProperty("release_key_password")
            }
        }
    }

    buildTypes {
        getByName("debug") {
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
        register("nightly") {
            signingConfig = signingConfigs.getByName("nightly")
            applicationIdSuffix = ".nightly"
        }
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = signingConfigs.getByName("release")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        viewBinding = true
        buildConfig = true
    }

    lint {
        disable.add("MissingTranslation")
    }

    namespace = "de.storchp.fdroidbuildstatus"

    applicationVariants.all {
        resValue("string", "applicationId", applicationId)

        if (name == "nightly" || name == "debug") {
            outputs.forEach { output ->
                output as com.android.build.gradle.internal.api.ApkVariantOutputImpl
                output.versionCodeOverride = gitVersion
                output.versionNameOverride = "${applicationId}_${output.versionCode}"
                output.outputFileName = "${applicationId}_${versionCode}.apk"
            }
        } else {
            outputs.forEach { output ->
                output as com.android.build.gradle.internal.api.ApkVariantOutputImpl
                output.outputFileName = "${applicationId}_${versionName}.apk"
            }
        }
    }

}

// Allow references to generated code
kapt {
    correctErrorTypes = true
}

dependencies {
    coreLibraryDesugaring(libs.desugar.jdk.libs)

    implementation(libs.activity)
    implementation(libs.activity.ktx)
    implementation(libs.retrofit)
    implementation(libs.converter.gson)
    implementation(libs.converter.jackson)
    implementation(libs.logging.interceptor)

    implementation(libs.jackson.dataformat.yaml)
    implementation(libs.jackson.module.kotlin)

    implementation(libs.lifecycle.viewmodel.ktx)
    implementation(libs.localbroadcastmanager)
    implementation(libs.appcompat)
    implementation(libs.preference.ktx)
    implementation(libs.recyclerview)
    implementation(libs.swiperefreshlayout)
    implementation(libs.material)
    implementation(libs.constraintlayout)
    implementation(libs.gson)
    implementation(libs.hilt.android)
    implementation(libs.hilt.work)
    implementation(libs.work.runtime.ktx)

    kapt(libs.hilt.compiler)
    kapt(libs.hilt.android.compiler)
    kapt(libs.dagger.hilt.compiler)

    testImplementation(libs.junit.jupiter.api)
    testImplementation(libs.junit.jupiter.params)
    testImplementation(libs.assertj.core)
    testImplementation(libs.mockito.core)
    testImplementation(libs.mockwebserver)
    testImplementation(libs.awaitility)
    testRuntimeOnly(libs.junit.jupiter.engine)

    androidTestImplementation(libs.junit)
    androidTestImplementation(libs.espresso.core)
    androidTestImplementation(libs.runner)
    androidTestImplementation(libs.android.test.core)
    androidTestImplementation(libs.junit.jupiter.api)
    androidTestImplementation(libs.junit.jupiter.params)
    androidTestImplementation(libs.assertj.core)
    androidTestRuntimeOnly(libs.android.test.runner)
}
